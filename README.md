A simple command line emoji finder written in [Racket][racket]. Usage:

```console
> emo cat
🐈‍⬛ 🐈 🐱 😾 😿 🙀 😽 😼 😻 😹 😸 😺
```

With descriptions:
```console
> emo -v cat
🐈‍⬛   black cat
🐈   cat
🐱   cat face
😾   pouting cat
😿   crying cat
🙀   weary cat
😽   kissing cat
😼   cat with wry smile
😻   smiling cat with heart-eyes
😹   cat with tears of joy
😸   grinning cat with smiling eyes
😺   grinning cat
```

Note that your terminal may not display all emoji correctly; nevertheless copy-pasting them to a fully emoji-aware program (like Firefox) will work fine.

The search looks for matches at the beginnings of words, and'ed to gether. Thus:

```console
> emo -v heart green
💚   green heart
> emo -v palms together
🤲🏻   palms up together: light skin tone
🤲🏿   palms up together: dark skin tone
🤲🏾   palms up together: medium-dark skin tone
🤲🏽   palms up together: medium skin tone
🤲🏼   palms up together: medium-light skin tone
🤲   palms up together
```

The emoji names are from:
- the description field of the [emoji-test.txt][emoji] published by unicode.org;
- the [emoji-data][emoji-data] project.

[emoji]: https://unicode.org/Public/emoji/14.0/emoji-test.txt
[emoji-data]: https://github.com/iamcal/emoji-data
[racket]: https://racket-lang.org

# Installation

```console
> raco pkg install emo
```
